package com.nexsoft.controller;

import com.nexsoft.model.Mahasiswa;
import com.nexsoft.model.MataPelajaran;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class NilaiController extends HttpServlet {

    public NilaiController() {
        super();
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        Mahasiswa mtModel = new Mahasiswa();

        try {
            Map where = new HashMap();
            where.put("id_mataPelajaran", Integer.parseInt(req.getParameter("id")));
            ResultSet nilai = mtModel.get_where("mahasiswas", where);

            req.setAttribute("data", nilai);

            RequestDispatcher rd = req.getRequestDispatcher("/Penilaian.jsp");
            rd.forward(req, res);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("id"));
        String nilai = req.getParameter("nilai");

        String semester = req.getParameter("semester");

        Mahasiswa userModel = new Mahasiswa();
        Map data = new HashMap();

        data.put("nilai",nilai);


        Map where = new HashMap();
        where.put("id", id);

        try {
            userModel.update("mahasiswas", data, where);

            res.sendRedirect("/tugasSSSS/ReadMahasiswaController");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
