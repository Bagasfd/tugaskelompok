package com.nexsoft.controller;

import com.nexsoft.model.MataPelajaran;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class CreateMataPelajaranController extends HttpServlet {

    public CreateMataPelajaranController() {
        super();
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
    {
        try {

            RequestDispatcher rd = req.getRequestDispatcher("/mataPelajaranCreate.jsp");

            rd.forward(req, res);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
    {
        try {
            String nama = req.getParameter("nama");


            MataPelajaran userModel = new MataPelajaran();
            Map data = new HashMap();

            data.put("nama",nama);

            userModel.insert("matapelajaran", data);
            res.sendRedirect("/tugasSSSS/ReadMataPelajaranController");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}