package com.nexsoft.controller;

import com.nexsoft.model.Pengajar;
import com.nexsoft.model.MataPelajaran;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class CreatePengajarController extends HttpServlet {

    public CreatePengajarController() {
        super();
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
    {

        MataPelajaran mtModel = new MataPelajaran();

        try {

            ResultSet mataPelajaran = mtModel.getUsers();

            req.setAttribute("mataPelajaran", mataPelajaran);

            RequestDispatcher rd = req.getRequestDispatcher("/PengajarCreate.jsp");

            rd.forward(req, res);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
    {
        try {
            String nama = req.getParameter("nama");
            String idMataPelajaran = req.getParameter("mataPelajaran");
            String tahunBergabung = req.getParameter("tahunBergabung");
            String jadwal = req.getParameter("jadwal");


            Pengajar userModel = new Pengajar();
            Map data = new HashMap();
            data.put("nama",nama);
            data.put("id_mataPelajaran", idMataPelajaran);
            data.put("tahunBergabung", tahunBergabung);
            data.put("jadwal", jadwal);

            userModel.insert("pengajar", data);
            res.sendRedirect("/tugasSSSS/ReadPengajarController");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}