package com.nexsoft.controller;

import com.nexsoft.model.MataPelajaran;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;

public class ReadMataPelajaranController extends HttpServlet {

    public ReadMataPelajaranController() {
        super();
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
    {

        MataPelajaran userModel = new MataPelajaran();

        ResultSet users = userModel.getUsers();

        req.setAttribute("mataPelajaran", users);
        RequestDispatcher rd = req.getRequestDispatcher("/mataPelajaranRead.jsp");

        rd.forward(req, res);
    }
}
