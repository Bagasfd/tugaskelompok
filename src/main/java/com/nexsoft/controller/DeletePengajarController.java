package com.nexsoft.controller;

import com.nexsoft.model.Pengajar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class DeletePengajarController extends HttpServlet {

    public DeletePengajarController() {
        super();
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
    {
        int id = Integer.parseInt(req.getParameter("id"));

        Pengajar userModel = new Pengajar();

        Map where = new HashMap();
        where.put("id", id);

        try {
            userModel.delete("pengajar", where);

            res.sendRedirect("/tugasSSSS/ReadPengajarController");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
