package com.nexsoft.controller;

import com.nexsoft.model.MataPelajaran;
import com.nexsoft.model.Mengajar;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;

public class ReadMengajarController extends HttpServlet {

    public ReadMengajarController() {
        super();
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
    {

        Mengajar userModel = new Mengajar();

        ResultSet users = userModel.getUsers();;
        req.setAttribute("data", users);
        RequestDispatcher rd = req.getRequestDispatcher("/mengajarRead.jsp");

        rd.forward(req, res);
    }
}
