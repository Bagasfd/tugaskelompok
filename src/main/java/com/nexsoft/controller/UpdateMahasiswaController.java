package com.nexsoft.controller;

import com.nexsoft.model.Mahasiswa;
import com.nexsoft.model.MataPelajaran;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class UpdateMahasiswaController extends HttpServlet {

    public UpdateMahasiswaController() {
        super();
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        Mahasiswa userModel = new Mahasiswa();
        MataPelajaran mtModel = new MataPelajaran();

        try {
            Map where = new HashMap();
            where.put("id", Integer.parseInt(req.getParameter("id")));
            ResultSet user = userModel.get_where("mahasiswas", where);
            ResultSet mataPelajaran = mtModel.getUsers();

            req.setAttribute("singleUser", user);
            req.setAttribute("mataPelajaran", mataPelajaran);

            RequestDispatcher rd = req.getRequestDispatcher("/mahasiswaUpdate.jsp");
            rd.forward(req, res);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("id"));
        String nama = req.getParameter("nama");
        String idMataPelajaran = req.getParameter("mataPelajaran");
        String jurusan = req.getParameter("jurusan");
        String fakultas = req.getParameter("fakultas");
        String semester = req.getParameter("semester");

        Mahasiswa userModel = new Mahasiswa();
        Map data = new HashMap();
        data.put("id_mataPelajaran", Integer.parseInt(idMataPelajaran));
        data.put("nama",nama);
        data.put("jurusan", jurusan);
        data.put("semester", semester);
        data.put("fakultas", fakultas);
        data.put("nilai", 0);

        Map where = new HashMap();
        where.put("id", id);

        try {
            userModel.update("mahasiswas", data, where);

            res.sendRedirect("/tugasSSSS/ReadMahasiswaController");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
