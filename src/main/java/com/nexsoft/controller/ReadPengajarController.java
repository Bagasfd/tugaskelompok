package com.nexsoft.controller;

import com.nexsoft.model.Pengajar;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;

public class ReadPengajarController extends HttpServlet {

    public ReadPengajarController() {
        super();
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
    {

        Pengajar userModel = new Pengajar();

        ResultSet users = userModel.getUsers();

        req.setAttribute("pengajar", users);
        RequestDispatcher rd = req.getRequestDispatcher("/pengajarRead.jsp");

        rd.forward(req, res);
    }
}
