package com.nexsoft.controller;

import com.nexsoft.model.Pengajar;
import com.nexsoft.model.MataPelajaran;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class UpdatePengajarController extends HttpServlet {

    public UpdatePengajarController() {
        super();
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        Pengajar userModel = new Pengajar();
        MataPelajaran mtModel = new MataPelajaran();

        try {
            Map where = new HashMap();
            where.put("id", Integer.parseInt(req.getParameter("id")));
            ResultSet user = userModel.get_where("pengajar", where);
            ResultSet mataPelajaran = mtModel.getUsers();

            req.setAttribute("singleUser", user);
            req.setAttribute("mataPelajaran", mataPelajaran);

            RequestDispatcher rd = req.getRequestDispatcher("/pengajarUpdate.jsp");
            rd.forward(req, res);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("id"));
        String nama = req.getParameter("nama");
        String idMataPelajaran = req.getParameter("mataPelajaran");
        String tahunBergabung = req.getParameter("tahunBergabung");
        String jadwal = req.getParameter("jadwal");

        Pengajar userModel = new Pengajar();
        Map data = new HashMap();
        data.put("id_mataPelajaran", Integer.parseInt(idMataPelajaran));
        data.put("nama",nama);
        data.put("tahunBergabung", tahunBergabung);
        data.put("jadwal", jadwal);

        Map where = new HashMap();
        where.put("id", id);

        try {
            userModel.update("pengajar", data, where);

            res.sendRedirect("/tugasSSSS/ReadPengajarController");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
