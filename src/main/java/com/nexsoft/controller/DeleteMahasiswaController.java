package com.nexsoft.controller;

import com.nexsoft.model.Mahasiswa;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class DeleteMahasiswaController extends HttpServlet {

    public DeleteMahasiswaController() {
        super();
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
    {
        int id = Integer.parseInt(req.getParameter("id"));

        Mahasiswa userModel = new Mahasiswa();

        Map where = new HashMap();
        where.put("id", id);

        try {
            userModel.delete("mahasiswas", where);

            res.sendRedirect("/tugasSSSS/ReadMahasiswaController");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
