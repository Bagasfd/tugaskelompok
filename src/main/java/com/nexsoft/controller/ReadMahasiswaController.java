package com.nexsoft.controller;

import com.nexsoft.model.Mahasiswa;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;

public class ReadMahasiswaController extends HttpServlet {

    public ReadMahasiswaController() {
        super();
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
    {

        Mahasiswa userModel = new Mahasiswa();

        ResultSet users = userModel.getUsers();

        req.setAttribute("mahasiswas", users);
        RequestDispatcher rd = req.getRequestDispatcher("/mahasiswaRead.jsp");

        rd.forward(req, res);
    }
}
