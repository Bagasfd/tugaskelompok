package com.nexsoft.controller;

import com.nexsoft.model.Mahasiswa;
import com.nexsoft.model.MataPelajaran;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class UpdateMataPelajaranController extends HttpServlet {

    public UpdateMataPelajaranController() {
        super();
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        MataPelajaran mtModel = new MataPelajaran();

        try {
            Map where = new HashMap();
            where.put("id", Integer.parseInt(req.getParameter("id")));
            ResultSet mataPelajaran = mtModel.get_where("matapelajaran", where);

            req.setAttribute("singleUser", mataPelajaran);

            RequestDispatcher rd = req.getRequestDispatcher("/mataPelajaranUpdate.jsp");
            rd.forward(req, res);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("id"));
        String nama = req.getParameter("nama");

        String semester = req.getParameter("semester");

        Mahasiswa userModel = new Mahasiswa();
        Map data = new HashMap();

        data.put("nama",nama);


        Map where = new HashMap();
        where.put("id", id);

        try {
            userModel.update("matapelajaran", data, where);

            res.sendRedirect("/tugasSSSS/ReadMataPelajaranController");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
