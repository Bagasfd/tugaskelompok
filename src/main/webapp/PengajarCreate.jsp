<%@ page import ="java.sql.*" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Create</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
</head>
<body>
    <div class="container my-5">
        <div class="row justify-content-center">
            <div class="col-md-4">
                <div class="card mt-5">
                    <div class="card-header text-center">
                        Create Data
                    </div>
                    <%

                        ResultSet mataPelajaran = (ResultSet) request.getAttribute("mataPelajaran");
                    %>
                    <form method="post" action="CreatePengajarController" class="card-body">
                        <div class="form-group">
                            <label for="fullname">Fullname</label>
                            <input class="form-control" type="text" name="nama">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Mata Pelajaran</label>
                            <select class="form-control" name="mataPelajaran" id="exampleFormControlSelect1">
                              <option>Pilih mata pelajaran</option>
                               <% while(mataPelajaran.next()) { %>
                                   <option value="<%= mataPelajaran.getInt("id") %>"><%= mataPelajaran.getString("nama") %></option>
                               <% } %>
                            </select>
                          </div>
                        <div class="form-group">
                            <label for="status">Tahun Bergabung</label>
                            <input class="form-control" type="text" name="tahunBergabung" >
                        </div>
                        <div class="form-group">
                            <label for="physics">Jadwal</label>
                            <input class="form-control" type="text" name="jadwal" >
                        </div>
                        <button type="submit" class="btn btn-primary btn-block">Create</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>