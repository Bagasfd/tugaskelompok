<%@ page import ="java.sql.*" %>
<%@ page import ="java.util.*" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Menu</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
</head>
<body>
    <div class="container my-5">
        <h1 class="mb-4">Daftar Mata Pelajaran</h1>
        <a class="btn btn-primary" href="/tugasSSSS/CreateMataPelajaranController">Tambah data</a>
        <a class="btn btn-warning" href="/tugasSSSS/success.jsp">Kembali</a>
        <div class="users mt-5">
            <div class="row">
                <div class="col-8">
                    <%
                        ResultSet result = (ResultSet) request.getAttribute("mataPelajaran");
                    %>
                        <table class="table">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col">id</th>
                                    <th scope="col">nama</th>
                                    <th scope="col">action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <% while(result.next()) { %>
                                    <tr>
                                        <th scope="row"><%= result.getString("id") %></th>
                                          <td><%= result.getString("nama") %></td>
                                          <td>
                                                <a href="/tugasSSSS/UpdateMataPelajaranController?id=<%= result.getString("id") %>" class="btn btn-sm btn-primary">Update</a>
                                                <form class="d-inline" method="post" action="DeleteMataPelajaranController">
                                                    <input type="hidden" name="id" value="<%= result.getString("id") %>">
                                                    <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Anda yakin ingin menghapus data ini ?')">Delete</button>
                                                </form>
                                          </td>
                                        </tr>
                                       <% } %>
                            </tbody>
                        </table>
                </div>
            </div>
        </div>
    </div>
</body>
</html>