<%@ page import ="java.sql.*" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Create</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
</head>
<body>
    <div class="container my-5">
        <div class="row justify-content-center">
            <div class="col-md-4">
                <div class="card mt-5">
                    <div class="card-header text-center">
                        Edit Data
                    </div>

                     <%
                            ResultSet singleUser = (ResultSet) request.getAttribute("singleUser");
                            singleUser.next();
                    %>

                    <form method="post" action="UpdateMataPelajaranController" class="card-body">
                       <input type="hidden" name="id" value="<%= singleUser.getString("id") %>">
                        <div class="form-group">
                            <label for="nama">Nama Mata Pelajaran</label>
                            <input class="form-control"  value="<%= singleUser.getString("nama") %>" type="text" name="nama">
                        </div>
                        <button type="submit" class="btn btn-primary btn-block">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>