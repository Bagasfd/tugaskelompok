<%@ page import ="java.sql.*" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
</head>
<body>
    <div class="container my-5">
        <div class="row justify-content-center">
            <div class="col-md-4">
                <div class="card mt-5">
                    <div class="card-header text-center">
                        Penilaian
                    </div>
                            <%
                                ResultSet data = (ResultSet) request.getAttribute("data");

                            %>
                    <form method="post" action="NilaiController" class="card-body">
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Daftar Siswa</label>
                            <select class="form-control" name="id" id="exampleFormControlSelect1">
                              <option>Pilih Siswa</option>
                               <% while(data.next()) { %>
                                     <option value="<%= data.getInt("id") %>"><%= data.getString("nama") %>
                             <% } %>
                            </select>
                          </div>
                        <div class="form-group">
                            <label for="status">Nilai</label>
                            <input class="form-control" type="text" name="nilai">
                        </div>

                        <button class="btn btn-primary btn-block">Nilai</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>